class ApplicationMailer < ActionMailer::Base
  default from: "noreply@paws.com"
  layout 'mailer'
end

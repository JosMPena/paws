class PostsController < ApplicationController
  include SessionsHelper, NotificationsHelper

  before_action :logged_in_user?, only: [:create, :destroy]
  before_action :correct_user, only: :destroy
  before_action :set_post, only: [:destroy, :show, :like]

  def index
    return unless logged_in?
    @post = current_user.posts.build
    # @feed_items = current_user.feed.page params[:page]
    @feed_items = Post.for_index(current_user).order('created_at DESC').page params[:page]
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save!
      redirect_to root_url
    else
      flash[:danger] = "Ups, we couldn't post that."
      render :index
    end
  end

  def destroy
    @post.destroy
    flash[:success] = "post deleted"
    redirect_to request.referrer || root_url
  end

  def show
    @post = Post.find(params[:id])
  end

  def like
    if current_user.voted_for? @post
      @post.unliked_by current_user
    else
      @post.liked_by current_user
      create_post_notification @post, "like"
    end
    # binding.pry
    respond_to do |format|
      format.html { redirect_to :back }
      format.js { render 'like' }
    end
    # render json: params
  end

  private

  def post_params
    params.require(:post).permit(:content, :image, :video, :title)
  end

  def correct_user
    @post = current_user.posts.find_by(id: params[:id])
    redirect_to root_url if @post.nil?
  end

  def set_post
    @post = Post.find(params[:id])
  end
end

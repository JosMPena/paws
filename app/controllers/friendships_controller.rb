class FriendshipsController < ApplicationController
  include NotificationsHelper
  before_action :get_other_user, except: [:index]

  def index
    @friends = current_user.friends.to_a
  end

  def request_friend
    if current_user.invite @other_user
      create_friend_notification(@other_user, 'request')
    else
      flash.now[:alert] = "We couldn't send a request to
        #{@other_user.pet_name}. Try again later"
    end
    respond_to do |format|
      format.html { redirect_to self }
      format.js {render 'actions'}
    end
  end

  def accept_friend
    friendship = Friendship.where(user_id: @other_user.id).where(friend_id: current_user.id)
    if current_user.accept_friend(friendship)
      create_friend_notification(@other_user, 'accept_request')
    else
      flash[:alert] = "An error has occurred, please try again later"
    end
    respond_to do |format|
      format.html { redirect_to root_path }
      format.js {render 'actions'}
    end
  end

  def reject_friend
    friendship = Friendship.where(user_id: @other_user.id).where(friend_id: current_user.id)
    if current_user.reject_friend(friendship.first)
        create_friend_notification(@other_user, 'reject_request')
    else
      flash[:alert] = "Ha ocurrido un error. Intenta de nuevo mas tarde"
    end
    respond_to do |format|
      format.html { redirect_to root_path }
      format.js {render 'actions'}
    end
  end

  def unfriend
    if current_user.unfriend(@other_user)
      current_user.remove_notification(@other_user)
    else
      flash[:alert] = "Ha ocurrido un error. Intenta de nuevo mas tarde"
    end
    respond_to do |format|
      format.html
      format.js {render 'actions'}
    end
  end

  private

  def get_other_user
    @other_user = User.find(params[:id])
  end

end

class NotificationsController < ApplicationController
  def open_content
    @notification = Notification.find(params[:id])
    @notification.update read: true
    if @notification.post
      redirect_to post_path @notification.post
    else
      @user = User.find(@notification.notified_by)
      redirect_to @user
    end
  end

  def index
    @notifications = last_notifications.page(params[:page]).per(10)
  end

  private

  def last_notifications
    current_user.notifications.order(created_at: :desc)
  end
end

class SessionsController < ApplicationController

  def create
    find_user('email', params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      #Log the user successfully and redirects to user page
      if @user.activated?
        log_in @user
        params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
        redirect_back_or root_url
      else
        message  = "Account not activated. Check your email for the activation link."
        message += "Check your email for the activation link."
        flash.now[:warning] = message
        redirect_back_or root_url
      end
    else
      #flash.now displays the error message only in current page (dissapears with new request) <= Cool Stuff xD
      flash.now[:danger] = 'Invalid email/password combination'
      render('new')
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

  def send_activation
    #TODO:60 re-send activation link
  end
end

class Post < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :notifications, dependent: :destroy

  acts_as_votable
  has_attached_file :image, styles: { :medium => "450x400" }
  has_attached_file :video, styles: {
    :medium => { :geometry => "450x400", :format => 'mp4' },
    :thumb => { :geometry => "100x100#", :format => 'jpg', :time => 10 }
    }, :processors => [:transcoder]

  scope :for_index, -> (user) {
    ids = user.friends.map {|f| f.id}
    ids << user.id
    where(user_id: ids)
    }

  validates :user_id, presence: true
  # validates :content, presence: true, length: { minimum: 3, maximum: 255 }
  validates_attachment :image, size: { in: 0..1.megabytes }, content_type: { content_type: /\Aimage\/.*\Z/ }
  validates_attachment :video, size: { in: 0..3.megabytes }

end

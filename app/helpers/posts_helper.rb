module PostsHelper
  #DONE:50 Display the number of likes as a link
  #DONE:40 open a panel by clicking the number of likes (jQuery Bootstrap)
  #TODO:0 Display the name of likers as links in the opened panel
  #DONE:10 hide and show the comment form

  def display_likers(post)
    list_likers(set_votes(post))
  end

  def display_likes(post)
    count_likers(set_votes(post))
  end

  def liked_post(post)
    return 'primary' if current_user.voted_for? post 
    'default'
  end

  def display_comments(post)
    pluralize post.comments.count, "Comentario"
  end

  private

  def list_likers(votes)
    user_names = []
    unless votes.blank?
      votes.voters.each do |voter|
        user_names.push(link_to voter.pet_name, user_path(voter), class: 'user-name')
      end
      user_names.to_sentence.html_safe
    end
  end

  def count_likers(votes)
    vote_count = votes.size
    pluralize vote_count, 'Guau'
  end

  def set_votes(post)
    post.votes_for.up.by_type("User")
  end

end

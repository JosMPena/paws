module NotificationsHelper
  def create_post_notification(post, type)
    return if post.user_id == current_user.id
    user = User.find(post.user_id)
    notification = user.notifications.build(
                          user_id: post.user.id,
                          notified_by_id: current_user.id,
                          post_id: post.id,
                          notice_type: type)
    notification.save!
  end

  def create_friend_notification(user, type)
    return if user == current_user
    notification = user.notifications.build(
                          user_id: user.id,
                          notified_by_id: current_user.id,
                          post_id: nil,
                          notice_type: type)
    notification.save!
  end

  def notification_description (notification)
    case notification.notice_type
      when "like"
        return "ha hecho Guau! en tu post"
      when "comment"
        return "ha comentado en tu post"
      when "request"
        return "te ha enviado una solicitud de amistad"
      when "accept_request"
        return "ha aceptado tu solicitud de amistad"
      when "reject_request"
        return "ha rechazado tu solicitud de amistad"
    end
  end
end

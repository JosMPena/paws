  $( document ).on('turbolinks:load', function() {

    // Adjust the size of the textarea dinamically
    $('.post-text').autosize();
  });

  // Clear the content of textarea after posting or close
  $(document).on('click', '#cancel-post', function(){
    var textarea = $('.post-text');
    textarea.val('');
  });

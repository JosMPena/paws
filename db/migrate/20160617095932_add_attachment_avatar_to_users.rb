class AddAttachmentAvatarToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :pet_image
    end
  end

  def self.down
    remove_attachment :users, :pet_image
  end
end

class AddAttachmentHumanImageToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :human_image
    end
  end

  def self.down
    remove_attachment :users, :human_image
  end
end
